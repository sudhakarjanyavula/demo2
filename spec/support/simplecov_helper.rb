require 'simplecov'
require 'active_support/core_ext/numeric/time'
module SimpleCovHelper
	def self.configure_profile
		SimpoleCov.configure do
			maximum_coverage_drop 0.02
			add_filter 'cache/'
			add_filter 'spec/'
			add_group 'Controllers', 'app/controllers'
			add_group 'Helpers', 'app/helpers'
			add_group 'Long files' do |src_file|
				src_file.lines.count > 100
			end
		end
	end

	def self.start!
		configure_profile
		SimpoleCov.start
	end
end